package com.darklight.educationalLocations;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Bundle;
import android.os.Looper;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.darklight.educationalLocations.models.Coordinates;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;

import org.osmdroid.config.Configuration;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapController;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.Marker;

public class MapFragment extends Fragment {

    private Coordinates coordinates;
    private String type, name;
    private Location location;
    private Marker current;
    private MapView mapView;

    public MapFragment(Coordinates coordinates, String type, String name) {
        this.coordinates = coordinates;
        this.type = type;
        this.name = name;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Configuration.getInstance().setUserAgentValue(BuildConfig.APPLICATION_ID);

        // Fix Android 10 sqlite
        Configuration.getInstance().load(getContext(), PreferenceManager.getDefaultSharedPreferences(getContext()));

        View rootView = inflater.inflate(R.layout.map_fragment, container, false);

        mapView = rootView.findViewById(R.id.mapView);
        MapController mapController = (MapController) mapView.getController();
        mapController.setCenter(new GeoPoint(1.214631, -77.278286));
        mapController.setZoom(15);
        mapView.setMultiTouchControls(true);

        // Load Zone Icon
        Bitmap bitmap = null;

        switch (type) {
            case "WiFi":
                bitmap = ((BitmapDrawable)getResources().getDrawable(R.drawable.wifi_location)).getBitmap();
                break;
            case "Digital":
                bitmap = ((BitmapDrawable)getResources().getDrawable(R.drawable.punto_digital_location)).getBitmap();
                break;
            case "School":
                bitmap = ((BitmapDrawable)getResources().getDrawable(R.drawable.school_location)).getBitmap();
                break;
        }

        bitmap = Bitmap.createScaledBitmap(bitmap, 150, 150, false);
        Drawable icon = new BitmapDrawable(getResources(), bitmap);

        // Get Location
        if(getContext().checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                getContext().checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(getContext(), "Location permission Error", Toast.LENGTH_SHORT).show();
        }
        else{
            // Location Provider && Location Request
            FusedLocationProviderClient fusedLocationProviderClient = new FusedLocationProviderClient(getActivity());
            LocationRequest locationRequest = new LocationRequest();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setFastestInterval(2000);
            locationRequest.setInterval(4000);

            // Current Location Marker
            current = new Marker(mapView);
            current.setTitle(getResources().getString(R.string.here));
            current.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
            mapView.getOverlays().add(current);

            // Request Location update
            fusedLocationProviderClient.requestLocationUpdates(locationRequest, new LocationCallback(){
                @Override
                public void onLocationResult(LocationResult locationResult) {
                    super.onLocationResult(locationResult);
                    location = locationResult.getLastLocation();
                    Log.e("LOCATION", "lat "+location.getLatitude()+" lon: "+location.getLongitude());
                    // Place current location Marker
                    current.setPosition(new GeoPoint(location.getLatitude(), location.getLongitude()));
                    mapView.invalidate();   // update markers view
                }
            }, Looper.getMainLooper());
        }

        // Place Zone Marker
        Marker zone = new Marker(mapView);
        zone.setIcon(icon);
        zone.setPosition(new GeoPoint(Double.valueOf(coordinates.getLatitude()), Double.valueOf(coordinates.getLongitude())));
        zone.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
        zone.setTitle(name);
        mapView.getOverlays().add(zone);
        mapView.invalidate();   // update markers view

        return rootView;
    }
}