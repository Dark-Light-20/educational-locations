package com.darklight.educationalLocations.datosapi;

import com.darklight.educationalLocations.models.DigitalZone;
import com.darklight.educationalLocations.models.SchoolZone;
import com.darklight.educationalLocations.models.WiFiZone;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;

public interface DatosAPIservice {

    @GET("pkga-gxrz.json")
    Call<ArrayList<WiFiZone>> getWifiZonesList();

    @GET("bc7q-iwhb.json")
    Call<ArrayList<DigitalZone>> getDigitalZonesList();

    @GET("gxwu-pgqr.json")
    Call<ArrayList<SchoolZone>> getSchoolZonesList();

}
