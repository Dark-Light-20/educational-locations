package com.darklight.educationalLocations;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.darklight.educationalLocations.datosapi.DatosAPIservice;
import com.darklight.educationalLocations.models.Coordinates;
import com.darklight.educationalLocations.models.DigitalZone;
import com.darklight.educationalLocations.models.SchoolZone;
import com.darklight.educationalLocations.models.WiFiZone;
import com.google.android.material.navigation.NavigationView;

import org.osmdroid.config.Configuration;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private static final String TAG = "EDUC";

    private Retrofit retrofit;

    private AboutFragment aboutFragment;
    private WiFiZonesFragment wiFiZonesFragment;
    private DigitalZonesFragment digitalZonesFragment;
    private SchoolZonesFragment schoolZonesFragment;

    private ArrayList<WiFiZone> wifiZones;
    private ArrayList<DigitalZone> digitalZones;
    private ArrayList<SchoolZone> schoolZones;

    private String lastFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        aboutFragment = new AboutFragment();
        wiFiZonesFragment = new WiFiZonesFragment();
        digitalZonesFragment = new DigitalZonesFragment();
        schoolZonesFragment = new SchoolZonesFragment();

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setItemIconTintList(null);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        retrofit = new Retrofit.Builder()
                .baseUrl("https://www.datos.gov.co/resource/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        getWiFiZonesData();

        if(savedInstanceState == null){
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                    wiFiZonesFragment, "FRAGMENT").commit();
            getSupportActionBar().setTitle(getString(R.string.wifi));
            lastFragment = "Wifi";
            navigationView.setCheckedItem(R.id.wifi_item);
        }

        // Ext-Memory and Location permissions
        if (getApplicationContext().checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                && getApplicationContext().checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                && getApplicationContext().checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && getApplicationContext().checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION},1);
        }

        Configuration.getInstance().setUserAgentValue(BuildConfig.APPLICATION_ID);
    }

    private void getWiFiZonesData() {
        DatosAPIservice service = retrofit.create(DatosAPIservice.class);
        Call<ArrayList<WiFiZone>> wifiZonesAnswerCall = service.getWifiZonesList();

        wifiZonesAnswerCall.enqueue(new Callback<ArrayList<WiFiZone>>() {
            @Override
            public void onResponse(Call<ArrayList<WiFiZone>> call, Response<ArrayList<WiFiZone>> response) {
                if (response.isSuccessful()) {
                    wifiZones = response.body();
                    Coordinates coordinates = null;
                    for(int i=0;i<wifiZones.size();i++){
                        WiFiZone wiFiZone = wifiZones.get(i);
                        coordinates = wiFiZone.getCoordinates();
                        Log.i(TAG, "WiFi Zone ==> "+wiFiZone.getName()+" | "
                                +wiFiZone.getDirection()+" ||: "+coordinates.getLatitude()+", "+coordinates.getLongitude());
                    }

                    wiFiZonesFragment.adapter.addWiFiZones(wifiZones);
                }
                else {
                    Log.e(TAG, " onResponse: "+response.errorBody());
                }
            }

            @Override
            public void onFailure(Call<ArrayList<WiFiZone>> call, Throwable t) {
                Log.e(TAG," onFailure: "+t.getMessage());
            }
        });
    }

    private void getDigitalZonesData(){
        DatosAPIservice service = retrofit.create(DatosAPIservice.class);
        Call<ArrayList<DigitalZone>> digitalZonesAnswerCall = service.getDigitalZonesList();

        digitalZonesAnswerCall.enqueue(new Callback<ArrayList<DigitalZone>>() {
            @Override
            public void onResponse(Call<ArrayList<DigitalZone>> call, Response<ArrayList<DigitalZone>> response) {
                if (response.isSuccessful()) {
                    digitalZones = response.body();
                    Coordinates coordinates = null;
                    for(int i=0;i<digitalZones.size();i++){
                        DigitalZone digitalZone = digitalZones.get(i);
                        coordinates = digitalZone.getCoordinates();
                        Log.i(TAG, "Digital Zone ==> "+digitalZone.getIdBeneficiario()+" | "
                                +digitalZone.getDirecciNPvd()+" ||: "+coordinates.getLatitude()+", "+coordinates.getLongitude());
                    }

                    digitalZonesFragment.adapter.addDigitalZones(digitalZones);
                }
                else {
                    Log.e(TAG, " onResponse: "+response.errorBody());
                }
            }

            @Override
            public void onFailure(Call<ArrayList<DigitalZone>> call, Throwable t) {
                Log.e(TAG," onFailure: "+t.getMessage());
            }
        });
    }

    private void getSchoolZonesData(){
        DatosAPIservice service = retrofit.create(DatosAPIservice.class);
        Call<ArrayList<SchoolZone>> schoolZonesAnswerCall = service.getSchoolZonesList();

        schoolZonesAnswerCall.enqueue(new Callback<ArrayList<SchoolZone>>() {
            @Override
            public void onResponse(Call<ArrayList<SchoolZone>> call, Response<ArrayList<SchoolZone>> response) {
                if (response.isSuccessful()) {
                    schoolZones = response.body();
                    Coordinates coordinates = null;
                    for(int i=0;i<schoolZones.size();i++){
                        SchoolZone schoolZone = schoolZones.get(i);
                        coordinates = schoolZone.getCoordinates();
                        Log.i(TAG, "School Zone ==> "+schoolZone.getNombreEstablecimientoEducativo()+" | "
                                +schoolZone.getDireccion()+" ||: "+coordinates.getLatitude()+", "+coordinates.getLongitude());
                    }

                    schoolZonesFragment.adapter.addSchoolZones(schoolZones);
                }
                else {
                    Log.e(TAG, " onResponse: "+response.errorBody());
                }
            }

            @Override
            public void onFailure(Call<ArrayList<SchoolZone>> call, Throwable t) {
                Log.e(TAG," onFailure: "+t.getMessage());
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
            return;
        }

        Fragment current = getSupportFragmentManager().findFragmentByTag("MAP");
        if (current != null && current.isVisible()){
            Fragment fragment = null;
            String title = "";
            switch (lastFragment){
                case "Digital":
                    getDigitalZonesData();
                    fragment = digitalZonesFragment;
                    title = getResources().getString(R.string.digital);
                    break;
                case "School":
                    getSchoolZonesData();
                    fragment = schoolZonesFragment;
                    title = getResources().getString(R.string.schools);
                    break;
                case "Wifi":
                    getWiFiZonesData();
                    fragment = wiFiZonesFragment;
                    title = getResources().getString(R.string.wifi);
                    break;
            }
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                    fragment).commit();
            getSupportActionBar().setTitle(title);
        }
        else    super.onBackPressed();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.about_item:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        aboutFragment).commit();
                getSupportActionBar().setTitle(getString(R.string.about));
                break;

            case R.id.wifi_item:
                getWiFiZonesData();
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        wiFiZonesFragment).commit();
                lastFragment = "Wifi";
                getSupportActionBar().setTitle(getString(R.string.wifi));
                break;

            case R.id.digital_item:
                getDigitalZonesData();
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        digitalZonesFragment).commit();
                lastFragment = "Digital";
                getSupportActionBar().setTitle(getString(R.string.digital));
                break;

            case R.id.schools_item:
                getSchoolZonesData();
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        schoolZonesFragment).commit();
                lastFragment = "School";
                getSupportActionBar().setTitle(getString(R.string.schools));
                break;

        }

        DrawerLayout drawerLayout = findViewById(R.id.drawer_layout);
        drawerLayout.closeDrawer(GravityCompat.START);

        return true;
    }
}
