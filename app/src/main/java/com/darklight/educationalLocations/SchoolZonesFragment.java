package com.darklight.educationalLocations;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.darklight.educationalLocations.recyclerView.SchoolZonesAdapter;

public class SchoolZonesFragment extends Fragment {
    SchoolZonesAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.schoolzones_fragment, container, false);

        RecyclerView recyclerView = view.findViewById(R.id.schoolzones_recycler_view);
        adapter = new SchoolZonesAdapter(getFragmentManager());
        recyclerView.setAdapter(adapter);
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);

        return  view;
    }
}
