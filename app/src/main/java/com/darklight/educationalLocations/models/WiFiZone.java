package com.darklight.educationalLocations.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WiFiZone {
    @SerializedName("direccion")
    @Expose
    private String direction;
    @SerializedName("punto_wifi")
    @Expose
    private String name;
    @SerializedName("coordenadas_puntos_wifi")
    @Expose
    private Coordinates coordinates;

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Coordinates getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(Coordinates coordinates) {
        this.coordinates = coordinates;
    }
}
