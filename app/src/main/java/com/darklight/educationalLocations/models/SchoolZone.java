package com.darklight.educationalLocations.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SchoolZone {
    @SerializedName("nomnbre_de_la_sede_educativa")
    @Expose
    private String nomnbreDeLaSedeEducativa;
    @SerializedName("direccion")
    @Expose
    private String direccion;
    @SerializedName("barrio")
    @Expose
    private String barrio;
    @SerializedName("nombre_establecimiento_educativo")
    @Expose
    private String nombreEstablecimientoEducativo;
    @SerializedName("zona")
    @Expose
    private String zona;
    @SerializedName("ubicacion_en_dos_columnas")
    @Expose
    private Coordinates coordinates;
    @SerializedName("codigo_dane")
    @Expose
    private String codigoDane;
    @SerializedName("sede")
    @Expose
    private String sede;

    public String getNomnbreDeLaSedeEducativa() {
        return nomnbreDeLaSedeEducativa;
    }

    public void setNomnbreDeLaSedeEducativa(String nomnbreDeLaSedeEducativa) {
        this.nomnbreDeLaSedeEducativa = nomnbreDeLaSedeEducativa;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getBarrio() {
        return barrio;
    }

    public void setBarrio(String barrio) {
        this.barrio = barrio;
    }

    public String getNombreEstablecimientoEducativo() {
        return nombreEstablecimientoEducativo;
    }

    public void setNombreEstablecimientoEducativo(String nombreEstablecimientoEducativo) {
        this.nombreEstablecimientoEducativo = nombreEstablecimientoEducativo;
    }

    public String getZona() {
        return zona;
    }

    public void setZona(String zona) {
        this.zona = zona;
    }

    public Coordinates getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(Coordinates coordinates) {
        this.coordinates = coordinates;
    }

    public String getCodigoDane() {
        return codigoDane;
    }

    public void setCodigoDane(String codigoDane) {
        this.codigoDane = codigoDane;
    }

    public String getSede() {
        return sede;
    }

    public void setSede(String sede) {
        this.sede = sede;
    }
}
