package com.darklight.educationalLocations.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DigitalZone {
    @SerializedName("id_beneficiario")
    @Expose
    private String idBeneficiario;
    @SerializedName("regi_n")
    @Expose
    private String region;
    @SerializedName("fase")
    @Expose
    private String fase;
    @SerializedName("integrador_de_servicio")
    @Expose
    private String integradorDeServicio;
    @SerializedName("departamento")
    @Expose
    private String departamento;
    @SerializedName("municipio")
    @Expose
    private String municipio;
    @SerializedName("direcci_n_pvd")
    @Expose
    private String direcciNPvd;
    @SerializedName("coordenadas_pvd_validadas")
    @Expose
    private Coordinates coordinates;
    @SerializedName("horario_atenci_n_del_pvd")
    @Expose
    private String horary;
    @SerializedName("tipologia")
    @Expose
    private String tipologia;
    @SerializedName("tipo_de_beneficiario")
    @Expose
    private String tipoDeBeneficiario;
    @SerializedName("estado_actual_de_la_carpeta")
    @Expose
    private String estadoActualDeLaCarpeta;
    @SerializedName("no_convenio")
    @Expose
    private String noConvenio;
    @SerializedName("fecha_de_convenio")
    @Expose
    private String fechaDeConvenio;
    @SerializedName("tipo_ejecutor")
    @Expose
    private String tipoEjecutor;
    @SerializedName("ejecutor")
    @Expose
    private String ejecutor;
    @SerializedName("el_punto_ya_fue_donado_si_no")
    @Expose
    private String elPuntoYaFueDonadoSiNo;
    @SerializedName("resoluci_n_de_baja")
    @Expose
    private String resoluciNDeBaja;
    @SerializedName("escritura_de_donaci_n")
    @Expose
    private String escrituraDeDonaciN;

    public String getIdBeneficiario() {
        return idBeneficiario;
    }

    public void setIdBeneficiario(String idBeneficiario) {
        this.idBeneficiario = idBeneficiario;
    }

    public String getFase() {
        return fase;
    }

    public void setFase(String fase) {
        this.fase = fase;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getIntegradorDeServicio() {
        return integradorDeServicio;
    }

    public void setIntegradorDeServicio(String integradorDeServicio) {
        this.integradorDeServicio = integradorDeServicio;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public String getDirecciNPvd() {
        return direcciNPvd;
    }

    public void setDirecciNPvd(String direcciNPvd) {
        this.direcciNPvd = direcciNPvd;
    }

    public Coordinates getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(Coordinates coordinates) {
        this.coordinates = coordinates;
    }

    public String getHorary() {
        return horary;
    }

    public void setHorary(String horary) {
        this.horary = horary;
    }

    public String getTipologia() {
        return tipologia;
    }

    public void setTipologia(String tipologia) {
        this.tipologia = tipologia;
    }

    public String getTipoDeBeneficiario() {
        return tipoDeBeneficiario;
    }

    public void setTipoDeBeneficiario(String tipoDeBeneficiario) {
        this.tipoDeBeneficiario = tipoDeBeneficiario;
    }

    public String getEstadoActualDeLaCarpeta() {
        return estadoActualDeLaCarpeta;
    }

    public void setEstadoActualDeLaCarpeta(String estadoActualDeLaCarpeta) {
        this.estadoActualDeLaCarpeta = estadoActualDeLaCarpeta;
    }

    public String getNoConvenio() {
        return noConvenio;
    }

    public void setNoConvenio(String noConvenio) {
        this.noConvenio = noConvenio;
    }

    public String getFechaDeConvenio() {
        return fechaDeConvenio;
    }

    public void setFechaDeConvenio(String fechaDeConvenio) {
        this.fechaDeConvenio = fechaDeConvenio;
    }

    public String getTipoEjecutor() {
        return tipoEjecutor;
    }

    public void setTipoEjecutor(String tipoEjecutor) {
        this.tipoEjecutor = tipoEjecutor;
    }

    public String getEjecutor() {
        return ejecutor;
    }

    public void setEjecutor(String ejecutor) {
        this.ejecutor = ejecutor;
    }

    public String getElPuntoYaFueDonadoSiNo() {
        return elPuntoYaFueDonadoSiNo;
    }

    public void setElPuntoYaFueDonadoSiNo(String elPuntoYaFueDonadoSiNo) {
        this.elPuntoYaFueDonadoSiNo = elPuntoYaFueDonadoSiNo;
    }

    public String getResoluciNDeBaja() {
        return resoluciNDeBaja;
    }

    public void setResoluciNDeBaja(String resoluciNDeBaja) {
        this.resoluciNDeBaja = resoluciNDeBaja;
    }

    public String getEscrituraDeDonaciN() {
        return escrituraDeDonaciN;
    }

    public void setEscrituraDeDonaciN(String escrituraDeDonaciN) {
        this.escrituraDeDonaciN = escrituraDeDonaciN;
    }
}
