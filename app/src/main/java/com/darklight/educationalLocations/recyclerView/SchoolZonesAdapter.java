package com.darklight.educationalLocations.recyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.darklight.educationalLocations.MapFragment;
import com.darklight.educationalLocations.R;
import com.darklight.educationalLocations.models.SchoolZone;

import java.util.ArrayList;

public class SchoolZonesAdapter extends RecyclerView.Adapter<SchoolZonesHolder>{

    private ArrayList<SchoolZone> schoolZones;
    private FragmentManager fragmentManager;
    private MapFragment mapFragment;

    public SchoolZonesAdapter(FragmentManager fragmentManager) {
        this.fragmentManager = fragmentManager;
        this.schoolZones = new ArrayList<>();
    }

    @NonNull
    @Override
    public SchoolZonesHolder onCreateViewHolder(@NonNull ViewGroup parent, final int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.schoolzone_item, parent, false);
        final SchoolZonesHolder schoolZonesHolder = new SchoolZonesHolder(view);

        schoolZonesHolder.getItemCardView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mapFragment = new MapFragment(schoolZones.get((int) (v.getTag())).getCoordinates(), "School",
                        schoolZones.get((int) (v.getTag())).getNombreEstablecimientoEducativo());

                fragmentManager.beginTransaction().replace(R.id.fragment_container,
                        mapFragment).commit();
            }
        });

        return schoolZonesHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull SchoolZonesHolder schoolZonesHolder, int position) {
        TextView name = schoolZonesHolder.getName();
        TextView id = schoolZonesHolder.getId();
        TextView direction = schoolZonesHolder.getDirection();
        TextView sede = schoolZonesHolder.getSede();

        String txtId = "#"+String.valueOf(position+1);

        name.setText(schoolZones.get(position).getNombreEstablecimientoEducativo());
        id.setText(txtId);
        direction.setText(schoolZones.get(position).getDireccion());
        sede.setText(schoolZones.get(position).getSede());

        schoolZonesHolder.getItemCardView().setTag(position);
    }

    @Override
    public int getItemCount() {
        return schoolZones.size();
    }

    public void addSchoolZones(ArrayList<SchoolZone> schoolZones) {
        this.schoolZones.addAll(schoolZones);
        notifyDataSetChanged();
    }
}
