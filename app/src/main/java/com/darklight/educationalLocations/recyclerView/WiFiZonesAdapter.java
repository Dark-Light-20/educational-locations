package com.darklight.educationalLocations.recyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.darklight.educationalLocations.MapFragment;
import com.darklight.educationalLocations.R;
import com.darklight.educationalLocations.models.WiFiZone;

import java.util.ArrayList;

public class WiFiZonesAdapter extends RecyclerView.Adapter<WiFiZonesHolder> {

    private ArrayList<WiFiZone> wiFiZones;
    private FragmentManager fragmentManager;
    private MapFragment mapFragment;

    public WiFiZonesAdapter(FragmentManager fragmentManager) {
        this.fragmentManager = fragmentManager;
        this.wiFiZones = new ArrayList<>();
    }

    @NonNull
    @Override
    public WiFiZonesHolder onCreateViewHolder(@NonNull ViewGroup parent, final int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.wifizone_item, parent, false);
        final WiFiZonesHolder wiFiZonesHolder = new WiFiZonesHolder(view);

        wiFiZonesHolder.getItemCardView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mapFragment = new MapFragment(wiFiZones.get((int) (v.getTag())).getCoordinates(), "WiFi",
                        wiFiZones.get((int) (v.getTag())).getName());

                fragmentManager.beginTransaction().replace(R.id.fragment_container,
                        mapFragment, "MAP").commit();
            }
        });

        return wiFiZonesHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull WiFiZonesHolder wiFiZonesHolder, int position) {
        TextView name = wiFiZonesHolder.getName();
        TextView id = wiFiZonesHolder.getId();
        TextView direction = wiFiZonesHolder.getDirection();

        String txtId = "#"+String.valueOf(position+1);

        name.setText(wiFiZones.get(position).getName());
        id.setText(txtId);
        direction.setText(wiFiZones.get(position).getDirection());

        wiFiZonesHolder.getItemCardView().setTag(position);
    }

    @Override
    public int getItemCount() {
        return wiFiZones.size();
    }

    public void addWiFiZones(ArrayList<WiFiZone> wiFiZones) {
        this.wiFiZones.addAll(wiFiZones);
        notifyDataSetChanged();
    }
}
