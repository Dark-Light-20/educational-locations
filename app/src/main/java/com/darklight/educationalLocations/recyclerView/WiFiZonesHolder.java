package com.darklight.educationalLocations.recyclerView;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.darklight.educationalLocations.R;

class WiFiZonesHolder extends RecyclerView.ViewHolder {

    private ImageView imageView;
    private TextView name, id, direction;
    private CardView itemCardView;

    WiFiZonesHolder(@NonNull View itemView) {
        super(itemView);

        itemCardView = itemView.findViewById(R.id.wifi_item);
        imageView = itemView.findViewById(R.id.wifi_item_image);
        name = itemView.findViewById(R.id.wifi_item_name);
        id = itemView.findViewById(R.id.wifi_item_id);
        direction = itemView.findViewById(R.id.wifi_item_direction);
    }

    public ImageView getImageView() {
        return imageView;
    }

    public void setImageView(ImageView imageView) {
        this.imageView = imageView;
    }

    public TextView getName() {
        return name;
    }

    public void setName(TextView name) {
        this.name = name;
    }

    public TextView getId() {
        return id;
    }

    public void setId(TextView id) {
        this.id = id;
    }

    public TextView getDirection() {
        return direction;
    }

    public void setDirection(TextView direction) {
        this.direction = direction;
    }

    public CardView getItemCardView() {
        return itemCardView;
    }

    public void setItemCardView(CardView itemCardView) {
        this.itemCardView = itemCardView;
    }
}
