package com.darklight.educationalLocations.recyclerView;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.darklight.educationalLocations.MapFragment;
import com.darklight.educationalLocations.R;
import com.darklight.educationalLocations.models.DigitalZone;

import java.util.ArrayList;

public class DigitalZonesAdapter extends RecyclerView.Adapter<DigitalZonesHolder>{

    private ArrayList<DigitalZone> digitalZones;
    private Context context;
    private FragmentManager fragmentManager;
    private MapFragment mapFragment;

    public DigitalZonesAdapter(Context context, FragmentManager fragmentManager) {
        this.context = context;
        this.fragmentManager = fragmentManager;
        this.digitalZones = new ArrayList<>();
    }

    @NonNull
    @Override
    public DigitalZonesHolder onCreateViewHolder(@NonNull ViewGroup parent, final int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.digitalzone_item, parent, false);
        final DigitalZonesHolder digitalZonesHolder = new DigitalZonesHolder(view);

        digitalZonesHolder.getItemCardView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mapFragment = new MapFragment(digitalZones.get((int) (v.getTag())).getCoordinates(), "Digital",
                        "ID: "+digitalZones.get((int) (v.getTag())).getIdBeneficiario());

                fragmentManager.beginTransaction().replace(R.id.fragment_container,
                        mapFragment).commit();
            }
        });

        return digitalZonesHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull DigitalZonesHolder digitalZonesHolder, int position) {
        TextView id_beneficiario = digitalZonesHolder.getId_beneficiario();
        TextView id = digitalZonesHolder.getId();
        TextView direction = digitalZonesHolder.getDirection();
        TextView type = digitalZonesHolder.getType();

        String txtId = "#"+String.valueOf(position+1);
        String txtBeneficiary = context.getResources().getString(R.string.beneficiary)+" "
                +digitalZones.get(position).getIdBeneficiario();

        id_beneficiario.setText(txtBeneficiary);
        id.setText(txtId);
        direction.setText(digitalZones.get(position).getDirecciNPvd());
        type.setText(digitalZones.get(position).getTipoDeBeneficiario());

        digitalZonesHolder.getItemCardView().setTag(position);
    }

    @Override
    public int getItemCount() {
        return digitalZones.size();
    }

    public void addDigitalZones(ArrayList<DigitalZone> digitalZones) {
        this.digitalZones.addAll(digitalZones);
        notifyDataSetChanged();
    }
}