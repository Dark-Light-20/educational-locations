package com.darklight.educationalLocations.recyclerView;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.darklight.educationalLocations.R;

class DigitalZonesHolder extends RecyclerView.ViewHolder {
    private ImageView imageView;
    private TextView id_beneficiario, id, direction, type;
    private CardView itemCardView;

    DigitalZonesHolder(@NonNull View itemView) {
        super(itemView);

        itemCardView = itemView.findViewById(R.id.digital_item);
        imageView = itemView.findViewById(R.id.digital_item_image);
        id_beneficiario = itemView.findViewById(R.id.digital_item_id_beneficiario);
        id = itemView.findViewById(R.id.digital_item_id);
        direction = itemView.findViewById(R.id.digital_item_direction);
        type = itemView.findViewById(R.id.digital_item_type);
    }

    public ImageView getImageView() {
        return imageView;
    }

    public void setImageView(ImageView imageView) {
        this.imageView = imageView;
    }

    public TextView getId_beneficiario() {
        return id_beneficiario;
    }

    public void setId_beneficiario(TextView id_beneficiario) {
        this.id_beneficiario = id_beneficiario;
    }

    public TextView getId() {
        return id;
    }

    public void setId(TextView id) {
        this.id = id;
    }

    public TextView getDirection() {
        return direction;
    }

    public void setDirection(TextView direction) {
        this.direction = direction;
    }

    public TextView getType() {
        return type;
    }

    public void setType(TextView type) {
        this.type = type;
    }

    public CardView getItemCardView() {
        return itemCardView;
    }

    public void setItemCardView(CardView itemCardView) {
        this.itemCardView = itemCardView;
    }
}
